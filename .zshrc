#########
# Alias #
#########

if [[ -e $HOME/.config/zsh/alias.zsh ]]; then
	source "$HOME/.config/zsh/alias.zsh"
else
	echo "Error: Couldn't find the alias file"
fi

########
# Tmux #
########

if [[ -z $TMUX ]]; then
		tmux;
fi

##############
# Completion #
##############

autoload -U compinit
compinit -u

zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:cdr:*:*' menu select

###########
# History #
###########

export SAVEHIST=1000
export HISTSIZE=1000
export HISTFILE="$HOME/.zsh_history"

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search

[[ -n "${key[Up]}" ]] && bindkey -- "${key[Up]}" up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search


###########
# Prompts #
###########

setopt PROMPT_SUBST

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '%F{green}+%f'
zstyle ':vcs_info:*' unstagedstr '%F{red}!%f'
zstyle ':vcs_info:*' formats 'on %F{cyan}%b%f %c%u'
zstyle ':vcs_info:*' actionformats 'on %F{cyan}%b|%a%f %c%u'
zstyle ':vcs_info:git+set-message:*' hooks git-untracked

function +vi-git-untracked() {
		emulate -L zsh
		if [[ -n $(git ls-files --exclude-standard --others 2> /dev/null)  ]]; then
				hook_com[unstaged]+='%F{yellow}?%f'
		fi
}

precmd () { vcs_info }
PS1='%F{blue}%1~%(?..%F{yellow}%B!%b)%f ${vcs_info_msg_0_}
%F{red}%B%(!.#.$)%b%f ' 

################
# Great powers #
################

setopt autocd				# .. is shortcut for cd ..
setopt autoparamslash		# tab completing directory appends a slash
setopt correct				# command auto-correction
setopt histignorealldups	# filter duplicates from history
setopt histignorespace		# don't record commands starting with space
setopt sharehistory			# share history across shells
