;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
black = #282828
red	=	#cc241d
green = #98971a
yellow = #d79921
blue = #458588
purple = #b16286
aqua = #689d6a
gray = #a89984
orange = #d65d0e
white = #ebdbb2
background = #282828
foreground = #ebdbb2
primary = #cc241d
secondary = #98971a
alert = #cc241d

[bar/laptop]
monitor = ${env:MONITOR:eDP-1}
width = 100%
height = 27
fixed-center = false

background = ${colors.background}
foreground = #ebdbb2

line-size = 4
line-color = %{colors.yellow}

padding-left = 0
padding-right = 0

module-margin-left = 1
module-margin-right = 2

font-0 = FontAwesome:size-11;3
font-1 = SourceCodePro:pixelsize=10;1

modules-left = i3
modules-center = date
modules-right = pulseaudio network

tray-position = right
tray-padding = 2
separator = |

[bar/screen]
monitor = ${env:MONITOR:HDMI-2}
width = 100%
height = 27
fixed-center = false

background = ${colors.background}
foreground = #ebdbb2

line-size = 4
line-color = %{colors.yellow}

padding-left = 0
padding-right = 5

module-margin-left = 1
module-margin-right = 2

font-0 = SourceCodePro:pixelsize=10;1
font-1 = FontAwesome:size-11;3

modules-left = i3
modules-center = date
modules-right = pulseaudio network

tray-position = right
tray-padding = 2

cursor-click = pointer
cursor-scroll = ns-resize

[module/date]
type = internal/date
interval = 1.0
date =  %d-%m-%Y
time =  %H:%M
label = %date% %time%

[module/network]
type = internal/network
interface = wlp3s0
interval = 1.0

label-connected = : %signal%% Connected to: %essid%  Ip: %local_ip%
label-connected-foreground = ${colors.green}
label-disconnected = No connexion
label-disconnected-foreground = ${colors.red}

[module/pulseaudio]
type = internal/pulseaudio
use-ui-max = false 
format-volume = <label-volume>
label-volume = Volume: %percentage%%
label-volume-foreground = ${colors.green}
label-muted-volume = Volume: muted
label-muted-foreground = ${colors.red} 

;label-muted =  
;ramp-volume-0 = 
;ramp-volume-1 =  
;ramp-volume-2 = 

[module/i3]
type = internal/i3
pin-workspaces = true
index-sort = true
enable-click = true
enable-scroll = false
wrapping-scroll = false
reverse-scroll = false
fuzzy-match = true
format = <label-mode><label-state>
label-mode-padding = 2
label-focused = %index%  %icon%
label-focused-padding = 2
label-unfocused = %index%  %icon%
label-unfocused-padding = 2
label-visible = %index%  %icon%
label-visible-padding = 2
label-urgent = %index%  %icon%
label-urgent-padding = 2
label-focused-foreground = ${colors.blue}
label-focused-background = ${colors.background}
label-focused-underline = ${colors.yellow}
label-visible-foreground = ${colors.blue}
label-unfocused-foreground = ${colors.gray}
label-urgent-foreground = ${colors.alert} 

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;
ws-icon-5 = 6;
ws-icon-6 = 7;
ws-icon-7 = 8;
ws-icon-8 = 9;

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[settings]
screenchange-reload = true

; vim:ft=dosini
