#########
# ALIAS #
#########

########
# Tmux #
########

# Session
# -------

# Tmux  new-session 

alias tsnew='tmux new-session -s'
alias tsconnect='tmux switch-client -t'
alias tslist='tmux list-sessions'
alias tsrename='tmux rename-session -t'

#############
# protonvpn #
#############

alias vpninit='sudo protonvpn init'
alias vpnc='sudo protonvpn connect'
alias vpncr='sudo protonvpn c -r'
alias vpncf='sudo protonvpn c -f'
alias vpncp2p='sudo protonvpn c --p2p'
alias vpncfc='sudo protonvpn c --cc'
alias vpncsc='sudo protonvpn c --sc'
alias vpnr='sudo protonvpn reconnect'
alias vpnd='sudo protonvpn disconnect'
alias vpns='sudo protonvpn status'

##########
# Pacman #
##########

alias pcs='sudo pacman -S'
alias pcr='sudo pacman -R'

######
# ls #
######

alias ls='ls --color'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias lal='ls -la'
alias lR='ls -R'

########
# grep #
########

alias grep='grep --color'

# mkidr
alias mk='mkdir -p'


# Git
alias gcl='git clone'
alias ga='git add'
alias gp='git push'
alias gcm='git commit -m'
alias gs='git status'
alias glf='git ls-files'
alias gc='git checkout'
alias gcb='git checkout -b'
alias gl='git log --pretty --graph'
alias gsa='git submodule add'
alias gm='git merge'


# Vim
alias vim='nvim'
alias svim='sudo vim'
alias v='v .'
alias vs='vim -S'

# systemctl
alias senable='sudo systemctl enable'
alias sdisable='sudo systemctl disable'
alias sstart='sudo systemctl start'
alias sstop='sudo systemctl stop'
alias sstatus='sudo systemctl status'


# Cd
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias /='cd /'
alias ~='cd ~'
