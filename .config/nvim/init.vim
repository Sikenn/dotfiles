"""""""""""
" Plugins "
"""""""""""

call plug#begin('~/.config/nvim/plugged')

Plug 'morhetz/gruvbox'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'sheerun/vim-polyglot'
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'pbondoer/vim-42header'
Plug 'christoomey/vim-tmux-navigator'

call plug#end()

"""""""""""""
" UI Config "
"""""""""""""

set syntax
set number
colorscheme gruvbox
set tabstop=4
set softtabstop=4

"""""""""""""
" Keymaping "
"""""""""""""

let mapleader=","

" Moving
noremap t h
noremap n l
noremap s j
noremap r k

noremap j t
noremap l c
noremap h r
noremap k s

" Pane
noremap <C-t> <C-w>h
noremap <C-s> <C-w>j
noremap <C-r> <C-w>k
noremap <C-n> <C-w>l

noremap vv :vsplit<CR>
noremap hh :split<CR>
noremap <C-w> :w<CR>
noremap <C-x> :wq<CR>
noremap <C-q> :q<CR>


inoremap sr <esc>
" edit  vimrc and zshrc
nnoremap <leader>ev :vsp ~/.config/nvim/init.vim<CR>
nnoremap <leader>ez :vsp ~/.zshrc<CR>
nnoremap <leader>sv :source ~/.config/nvim/init.vim<CR>
nnoremap <leader>sz :source ~/.zshrc <CR>
" save session
nnoremap <leader>s :mksession<CR>

"""""""""""""
" ctrlp.vim "
"""""""""""""

" Exlude files and directories using Vim's wildignore
set wildignore+=*.swp,*.zip
" ignore files in .gitignore
let g:ctrlp_user_command = ['.git', 'cd %s %% git ls-files -co --exclude-standard']

""""""""""""
" nerdtree "
""""""""""""

" keybinding
map <C-f> :NERDTreeToggle<CR>
let NERDTreeMenuDown='s'
let NERDTreeMenuUP='r'
let NERDTreeMapOpenVSplit='v'
let NERDTreeMapOpenSplit='h'
let NERDTreeMapRefresh='d'

" Do not open Nerdtree on saved session
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && v:this_session == "" | NERDTree | endif
" Close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"""""""""""
" airline "
"""""""""""

let g:airline_theme='gruvbox'
